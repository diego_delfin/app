(function() {
	'use strict';

	angular
		.module('apparg')
		.constant('CONSTANT_VAR',{

			URL_BASE : 'http://locahost:8000/',
			URL_BASE_API : 'http://localhost:8000/api/'

			//"URL_BASE" : "http://app-prelacteal-pacification.cfapps.io/",
			//"URL_BASE_API" : "http://app-prelacteal-pacification.cfapps.io/api/"
		});
})();