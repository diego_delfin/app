(function() {
	'use strict';
	angular
	.module('apphome')
	.controller('HomeController',HomeController)
	.controller('ModalInstanceCtrl',ModalInstanceCtrl);

	HomeController.$inject = ['$scope','$location','$localStorage','$uibModal'];	
	function HomeController($scope,$location,$localStorage,$uibModal) {
		var vm = this ;
 		var modalInstace;

		vm.userlogueado = $localStorage.currentUser.username;
		vm.productos = [
			{'product_cod':'FFF1','product_name':'Mouse','product_desc':'Aparato de informatica','product_category':'Computo','product_stock':30,'product_price':60},
			{'product_cod':'FFF2','product_name':'Monitor','product_desc':'Aparato de informatica','product_category':'Computo','product_stock':50,'product_price':100},
			{'product_cod':'FFF3','product_name':'CPU','product_desc':'Aparato de informatica','product_category':'Computo','product_stock':60,'product_price':700},
			{'product_cod':'FFF4','product_name':'Teclado','product_desc':'Aparato de informatica','product_category':'Computo','product_stock':10,'product_price':400}
		];

		vm.open_addProduct = function() {
			$scope.prod = {};
			modalInstace = $uibModal.open({
				templateUrl : 'ModalAddEditProducto.html',
				controller : ModalInstanceCtrl,
				resolve : {
					data_product : function() {
						return $scope.prod;
					}
				}
			});
		};

		vm.update_editProduct = function(product) {
			$scope.prod = product;
			console.log('scope', $scope.prod);
			modalInstace = $uibModal.open({
				templateUrl : 'ModalAddEditProducto.html',
				controller : ModalInstanceCtrl,
				resolve : {
					data_product : function() {
						return $scope.prod;
					}
				}
			});
		};

		vm.delete_deleteProduct = function(id) {
			console.log('edit',id);
			modalInstace = $uibModal.open({
				templateUrl : 'ModalDeleteProducto.html'
			});
		};

		vm.logout = function () {
			console.log('aqui');
			delete $localStorage.currentUser;
			$location.path('/login');
		};
	}

	ModalInstanceCtrl.$inject = ['$scope','$uibModalInstance','data_product'];
	function ModalInstanceCtrl($scope,$uibModalInstance,data_product) {
	
		$scope.templateproduct = data_product;
		
		$scope.submit_AddProduct = function(product){
			console.log('prod', product);
		};

		$scope.close_AddProduct = function() {
			$uibModalInstance.dismiss('cancel');
		}

	}




})();