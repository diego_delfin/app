(function(){
	'use strict';

	angular.module('apparg',[
		'ngRoute',
		'ngStorage',
		'ui.bootstrap',
		'angular-spinkit',
		'applogin',
		'apphome'
		]);
})();
