(function(){
	'use strict';

	angular
		.module('applogin')
		.controller('LoginController',LoginController);

	LoginController.$inject=['$http','$location','$localStorage','LoginFactory'];

	function LoginController($http,$location,$localStorage,LoginFactory){

		var vm = this;

	 	vm.flash = {};
		vm.flash.result = false;
		vm.flash.type='';

		vm.login = login;
		vm.authmessage = '';

		function login() {

			vm.dataLoading = true;

			var dataUser = {
				'username': vm.username,
				'password' : vm.password
			};

			LoginFactory.signIn(dataUser).then(function(res){
				vm.dataLoading = false;
				if(res.status === 1) {
       
					$localStorage.currentUser =  { username : vm.username, token : res.resp.jwt };
					$http.defaults.headers.common.Authorization = 'Bearer' + res.resp.jwt;
				 	$location.path('/home');
				 	
				} else {
       				delete $localStorage.currentUser;
					vm.flash.result = true;
					vm.authmessage = 'El usuario y la contraseña que ingresaste no coinciden.';
					vm.flash.type = 'error';

				}
			});


		}
	}

})();