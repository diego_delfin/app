(function(){
	'use strict';

	angular
		.module('applogin')
		.factory('LoginFactory',LoginFactory);

	LoginFactory.$inject = ['$http','CONSTANT_VAR'];

	function LoginFactory($http, CONSTANT_VAR) {

		return {
			signIn : signIn 
		};


		function signIn(data) {
			console.log('data',data);
			return $http.post(CONSTANT_VAR.URL_BASE_API+'user/token',data).then(handleSuccess,handleError('Usuario o Password incorrectos'));
		}


		function handleSuccess(res) {
			return res.data;
		}

		function handleError(error) {
			return function () {
				return { success : false , message : error };
			};
		}
	}
})();