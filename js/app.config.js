(function(){
	'use strict';

	angular
		.module('apparg')
		.config(config);

	function config($routeProvider) {

		$routeProvider
			.when('/login', {
				controller: 'LoginController',
				templateUrl : 'partials/login.html'
			})
			.when('/home',{
				controller : 'HomeController',
				templateUrl : 'partials/home.html'
			})
			.otherwise({
				redirectTo: '/login'
			});
	}

})();