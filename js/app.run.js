(function(){
	'use strict';

	angular
		.module('apparg')
		.run(runBlock);

		runBlock.$inject = ['$rootScope', '$http','$location','$localStorage'];

		function runBlock($rootScope, $http, $location, $localStorage) {

			if($localStorage.currentUser) {
				var tokenBearer = ['Bearer',$localStorage.currentUser.token];
				console.log('pedir permiso',tokenBearer.join(" "));
				$http.defaults.headers.common.Authorization = tokenBearer.join(" ");
			}

			var rutasPrivadas = ['/login','/home'];
			$rootScope.$on('$routeChangeStart', function () {
				if(!$localStorage.currentUser){
            		$location.path("/login");
        		}
		        //en el caso de que intente acceder al login y ya haya iniciado sesión lo mandamos a la home
		        if(($location.path() === '/login') && $localStorage.currentUser){
		            $location.path("/home");
		        }
			});
		}
})();