<?php

require 'vendor/autoload.php';
use \Firebase\JWT\JWT;

define('SECRET_KEY', 'COQUINAOREJITAS');
define('ALGORITHM', 'HS512');

$app = new \Slim\Slim();

function getConnection(){

	$usuario = 'root';
	$password = '';
	//$usuario = 'b0545828d756bf';
	//$password = '7f99e7c9';
	try {
		$db = new PDO('mysql:host=localhost;dbname=apparg',$usuario,$password);
		//$db = new PDO('mysql:host=us-cdbr-iron-east-04.cleardb.net;dbname=ad_22be7b779431776',$usuario,$password);
		//$db = new mysqli("us-cdbr-iron-east-04.cleardb.net","b0545828d756bf","7f99e7c9","ad_22be7b779431776");
	} catch (PDOException $e){
		$db = null;
	}
	
	return $db;
}

$db = getConnection();


$app->get('/hola', function() use ($db) {
	echo 'data';
	phpinfo();
	//var_dump($db);
});


$app->post('/user/token', function () use ($app,$db) {

	if(!is_null($db)) {

		$request = $app->request();
		$data = json_decode($request->getBody());


		$username = $data->username;
		$password = md5($data->password);
		
		$result = $db->prepare('SELECT id,username FROM users WHERE username = :username AND password = :password');
		$result->bindParam(':username', $username);
		$result->bindParam(':password', $password);

		$result->execute();

		$res = $result->fetchAll(PDO::FETCH_ASSOC);

		if(count($res) > 0) {

			$tokenId = base64_encode(mcrypt_create_iv(32));
			$issuedAt = time();
			$notBefore = $issuedAt + 10;
			$expire = $notBefore + 7200;
			$serverName = "http://localhost:8000/api/";

			$row = $res[0];
			
			$data = [
				'iat' => $issuedAt,
				'jti' => $tokenId,
				'iss' => $serverName,
				'nbf' => $notBefore,
				'exp' => $expire,
				'data' => [
					'id' => $row['id'],
					'username' => $row['username']
					]
				];

			$secretKey = base64_decode(SECRET_KEY);
			$jwt = JWT::encode(
				$data,
				$secretKey,
				ALGORITHM
				);
			$unencodedArray = ['jwt' => $jwt];
			$response = array("status" => 1, "resp" => $unencodedArray);

		} else{
			$response = array("status" => 0, "msg" => "Contraseña o Password Invalidos");
		}
	} else {

		$response = array('status' => 401,"msg" => "Error en la base de datos" );
	}

		header('Content-Type: application/json');
		echo json_encode($response);
	
});


$app->get('/hello/:name', function ($name) {
    echo "Hello delfin , $name";
});

$app->get('/', function(){
	echo "hola mundo";
});

$app->run();

/* 

https://github.com/ttkalec/laravel5-angular-jwt/blob/master/app/Http/routes.php
https://www.toptal.com/web/cookie-free-authentication-with-json-web-tokens-an-example-in-laravel-and-angularjs
http://jasonwatmore.com/post/2015/03/10/AngularJS-User-Registration-and-Login-Example.aspx
https://github.com/cornflourblue/angular-registration-login-example
https://docs.angularjs.org/api/ngRoute/provider/$routeProvider
https://github.com/johnpapa/angular-styleguide/blob/master/a1/i18n/es-ES.md#controladores
https://docs.angularjs.org/guide/di
http://docs.slimframework.com/request/variables/
http://docs.slimframework.com/start/get-started/
*/